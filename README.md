# Elastic Learning

[Install Elasticsearch with Docker](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html)

[Running Kibana on Docker](https://www.elastic.co/guide/en/kibana/current/docker.html)
